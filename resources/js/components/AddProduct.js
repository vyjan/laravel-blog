import React, { Component } from 'react';

class AddProduct extends Component {

  constructor(props) {
    super(props);
    this.state = {
      newProduct: {
        title: '',
        description: '',
        availability: 0,
        price: 0
      }
    };
    // boilerplate code for binding methods with `this`
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInput  = this.handleInput.bind(this);
  }

  // this method dynamically accepts input and stores it in the state
  handleInput(key, e) {
    // duplicating and updating the state
    var state = Object.assign({}, this.state.newProduct);
    state[key] = e.target.value;
    this.setState({newProduct: state});
    // console.log(key + ': ' + state[key]);
  }

  // this method is invoked when submit button is pressed
  handleSubmit(e) {
    // interrupt


    // prevents page reload
    e.preventDefault();
    // call bak to onAdd, current state is passed as a param
    // console.log(this.state.newProduct);
    this.props.onAdd(this.state.newProduct);
  }

  render() {
    const divStyle = { }

    return (
      <div>
        <h2>Add new Product</h2>
        <div style={divStyle}>
          { /* control is passed to handleSubmit */}
          <form onSubmit={this.handleSubmit}>
            <label> Title:
              { /* on every keystroke, the handleInput is invoked */ }
              <input type="text" onChange={(e)=>this.handleInput('title',e)} />
            </label>
            <label> Description:
              <input type="text"  onChange={(e)=>this.handleInput('description',e)} />
            </label>
            <label> Availability:
              <input type="text"  onChange={(e)=>this.handleInput('availability',e)} />
            </label>
            <label> Price:
              <input type="text"  onChange={(e)=>this.handleInput('price',e)} />
            </label>

            <input type="submit" value="Submit" />
          </form>
        </div>
      </div>
    );
  }
}

export default AddProduct;
