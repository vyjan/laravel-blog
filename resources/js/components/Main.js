import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Product from './Product';
import AddProduct from './AddProduct';

/* Main Component */
class Main extends Component {
  constructor() {
    // initialize the state in the constructor
    super();
    this.state = { products: [], currentProduct:null };
    this.handleAddProduct = this.handleAddProduct.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
  /* componentDidMount() is a lifecycle method
   * that gets called after the component is rendered
   */
  componentDidMount() {
    // fetch API in action
    fetch('/api/products')
      .then(response=> {
        return response.json();
      })
      .then(products=> {
        // fetched product is stored in the state
        this.setState({products});
      });
  }

  renderProducts() {
    return this.state.products.map(product=> {
      return (
        //this.handleClick() method is invoked onClick.
           <li onClick={
               () =>this.handleClick(product)} key={product.id} >
               { product.title }
           </li>
      );
    })
  }

  handleClick(product) {
    // handleClick is used to set the state
    this.setState({currentProduct: product});
  }

  handleAddProduct(product) {
    product.price = Number(product.price);
    fetch('/api/products/', {
      method: 'post',
      // headers are important
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(product)
    })
    .then(response=>{
      return response.json();
    })
    .then(data=> {
      this.setState((prevState)=> ({
        products: prevState.products.concat(data),
        currentProduct: data
      }));
    });
  }

  handleDelete() {
    console.log(this.state.currentProduct);
    const currentProduct = this.state.currentProduct;
    fetch('/api/products/' + this.state.currentProduct.id,
      { method: 'delete'})
      .then(response=> {
        var array = this.state.products.filter(function(item) {
          return item != currentProduct;
        });
        this.setState({products: array, currentProduct: null});
      });
  }

  handleUpdate(product) {
    // const currentProduct = this.state.currentProduct;
    // fetch('/api/products/' + this.state.currentProduct.id, {
    //   method: 'put',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json'
    //   },
    //   body: JSON.stringify(product);
    // })
    // .then(response=>{
    //   return response.json();
    // })
    // .then(data=> {
    //   var array = this.state.products.filter(function(item) {
    //     return item!=currentProduct;
    //   });
    //   this.setState((prevState)=>({
    //     products: array.concat(product),
    //     currentProduct: product
    //   }));
    // });
  }

  render() {
  return (
    /* The extra divs are for the css styles */
      <div>
          <div></div>
          <div>
          <AddProduct onAdd={this.handleAddProduct} />
           <h3> All products </h3>
            <ul>
              { this.renderProducts() }
            </ul>
          </div>
          <div>
            <button onClick={this.handleDelete} type="button">Delete</button>
          </div>
          <div>
            <Product product={this.state.currentProduct} />
          </div>
      </div>
    );
  }
}

if (document.getElementById('root')) {
    ReactDOM.render(<Main />, document.getElementById('root'));
}
